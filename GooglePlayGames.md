##### Setup Google signing in via Play Games Plugin
  0. Có hướng dẫn chính thức từ trang chủ android: https://developer.android.com/games/pgs/unity/unity-start . Cân nhắc làm theo hướng dẫn chính thức đó thay vì làm theo các bước trong hướng dẫn này (từ bước 1 trở đi)
  1. Tải plugin từ https://github.com/playgameservices/play-games-plugin-for-unity
  2. Trên google play console, truy cập giao diện của app, mục Play Games Service, tạo mới game project và link với GCP project tương ứng
     (nếu tạo xong mà chưa publish thì chỉ có tài khoản test mới login đc)

      Lưu lại app id

      Config OAuth consent screen:
       - Vào https://console.developers.google.com, chọn đúng project, tìm mục `Google Auth Platform > Overview` vào tạo 1 cái, dạng external rồi publish. Nếu muốn dùng email khác để làm User support email thì phải add email đó vào google cloud project với quyền owner, sau đó đăng nhập vào email đó rồi tạo consent screen . Hoặc tạo mail group rồi add mail khác vào group rồi dùng group mail để config.

      Tạo Android Credential 
      - (có vẻ là ko tạo đc OAuth client nếu chưa up file build lên Play Console) (nên up build lên trc bởi hệ thống sẽ cần dùng đến keystore để tạo SHA, nếu nhỡ tạo Android Credential rồi mà chưa up build thì hệ thống sẽ tự sinh 1 cái credential dang dở và sẽ khiến cho việc tạo credential sau này bị báo là trùng: 'The request failed because the Android package name and fingerprint are already in use'. Lúc đó sẽ phải remove game project đi bằng cách vào giao diện config game service trên google play console, phần Configuration, kéo xuống dưới cùng có nút Delete game project)
       - Tạo mới credential, type là Android
         - Mục OAuth client, tạo mới mới theo hướng dẫn

      Trong trường hợp link app với game service đã có chứ ko phải là link với game service mới tạo lần đầu, cần phải tạo 2 Android credential tương ứng với 2 sha1 fingerprint (app signing và upload) (tìm trong giao diện play console, mục Setup > App signing)
      
      Trong quá trình tạo Android Credential, cần phải add fingerprint vào app trong Project settings của firebase, ko nhớ rõ thao tác này thực hiện ở bước nào, và bởi fingerprint này đi theo app nên nếu add app mới vào project thì cũng phải bổ sung.

      Link service với app. Trong quá trình này nếu có thông báo là api đang bị tắt thì bật lên lại.

      Trạng thái phải là ready to test

      Add tài khoản test vào danh sách tester ở `Testing > Testing access > Add testers`
  3. Add tài khoản google đang login trên device vào danh sách tài khoản test trên google play console
     (nếu có tài khoản nào khác đang cùng login trên device thì logout 
	 để chỉ có 1 tài khoản duy nhất là tài khoản test đang login vào device)
  4. Vào https://console.developers.google.com,
     chọn đúng project,
	 tìm mục `apis & services > credentials`
	 ở khu vực `oauth 2.0 client id` tìm `web client`,
	 bấm vào để copy client id, nếu chưa có thì tạo mới. Nếu trong firebase bật Authentication provider Google thì hệ thống sẽ tự tạo ra web client id. 

  5. Vào unity mở `window > google play game > setup > android setup`
     
     mục `resource definition`, điền thông tin sau 
     (thay thế app id bằng app id lấy ở bước 2) 
     (thông tin này có thể tạo ra bằng cách tạo 1 `Achievement` ở bước 2, sau đó bấm `Get resources`) 
     ![alt text](images/AchievementResources.png "Title Text")
      ```xml
      <?xml version="1.0" encoding="utf-8"?>
      <!--
      Google Play game services IDs.
      Save this file as res/values/games-ids.xml in your project.
      -->
      <resources>
        <!-- app_id -->
        <string name="app_id" translatable="false">406607822278</string>
        <!-- package_name -->
        <string name="package_name" translatable="false">com.Bravestars.Stickman.FightingGames.ShadowOfDeath2</string>
        <!-- achievement 1 -->
        <string name="achievement_1" translatable="false">CgkIxvvb3eoLEAIQAw</string>
      </resources>
      ```
     mục `web app client id`, điền id lấy ở bước 4
  6. Lúc build ra thì phải sign apk bằng đúng key đã sử dụng để sign app khi up lên google play
  7. Khi cần publish để tài khoản thường (ko phải tester) cũng có thể login được thì phải publish app tạo ở mục 2.
  8. Config Firebase Authentication: (tham khảo https://firebase.google.com/docs/auth/unity/play-games)
      
      Trong Firebase console, vào Authentication > Sign-in method:
         
      - Bật Google, mục Web SDK configuration, lưu lại id và secret
      - Bật Play games, điền id và secret ở bước trên vào
  9. Init plugin và sign in trong code bằng cách làm theo hướng dẫn trong 
     https://github.com/playgameservices/play-games-plugin-for-unity 
     mục `Configuration & Initialization Play Game Services` và `Sign in`
     
     Nếu cần token để đăng nhập vào Firebase Authentication thì sau khi có callback của sign in, 
     đợi vài giây rồi gọi hàm get id token (mục `Retrieving player's ID Token`) 
     rồi dùng token này truyền vào hàm authenticate của FirebaseAuth, 
     hàm này có 2 params là idToken và accessToken, chỉ điền idToken còn để null cho accessToken

     Firebase Authentication mặc định ko cho phép sử dụng cùng 1 email cho nhiều tài khoản, để thay đổi thì vào `Firebase Console > Develop > Authentication > Sign-in method > Advanced`

##### Error
- Khi đọc logcat thấy thông báo lỗi `authentication failed developer error`: kiểm tra config `web app client id` ở bước 5 đã đúng chưa. Tham khao them https://answers.unity.com/questions/1357666/google-play-games-services-authentication-fails.html
- "Chạy thêm cái google() trong dependency của gradle với activate cái androidx xong cài thêm androidx trong plugins. Cái thiếu chính là install package nó ko cài androidx cho mình và cũng ko báo lỗi chỗ đó." - chú Kiến